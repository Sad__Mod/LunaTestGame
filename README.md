# Luna's Racer
> Luna's Racer (previously known as 'Luna Test game') - is a racing competitive game, orientated for Android devices, about picking 'coins' and multiplying it by passing through 'rings' making the total score higher.


<!-- [![NPM Version][npm-image]][npm-url] -->
<!-- [![Build Status][travis-image]][travis-url] -->
<!-- [![Downloads Stats][npm-downloads]][npm-url] -->


![](header.png)

## Installation

Check 'Releases' for link on most recent '.apk' build, or build it yourself by cloning the project.

## Release History

Alpha:
* 0.2:
    * Gameplay mechanic added;
    * music added;
    * tutorial added;
    * translation improved;
    * save system improved.
* 0.1:
    * Game manager added;
    * Language manager added (Currently supported: English and Russian);
    * Save system;
    * Smooth screen transitions.

## Meta

You can find me on:
* LinkedIn – [@SadMod](https://www.linkedin.com/in/sadmod/);<br>
or <br>
* look for other contacts on my [site](https://maksimukirill.wixsite.com/video-resume).

<!-- ## Contributing

1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request -->

<!-- Markdown link & img dfn's -->
<!-- [npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square -->
<!-- [npm-url]: https://npmjs.org/package/datadog-metrics -->
<!-- [npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square -->
<!-- [travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square -->
<!-- [travis-url]: https://travis-ci.org/dbader/node-datadog-metrics -->
<!-- [wiki]: https://github.com/yourname/yourproject/wiki -->
