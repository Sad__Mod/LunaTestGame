using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas), typeof(CanvasGroup))]
public class MessageWindowController : MonoBehaviour
{
    [SerializeField] private List<GameObject> windows = new List<GameObject>();

    public int CurrentPage
    {
        get => _currentPage;
        set
        {
            if (value < 0)
            {
                Debug.LogError($"{nameof(CurrentPage)} cannot be less than 0.");
                return;
            }

            if (value > windows.Count - 1)
            {
                Debug.LogError($"{nameof(CurrentPage)} cannot be greater than windows count.");
                return;
            }
            _currentPage = value;

            SetActiveWindow(_currentPage);
        }
    }

    [SerializeField] private int _currentPage = 0;

    public bool ListCycled = false;

    void Awake()
    {
        windows.AddRange(GetComponentsInChildren<Transform>(true).Where(obj => obj.parent == transform).Select(obj => obj.gameObject).ToList());
    }

    public void @Reset()
    {
        CurrentPage = 0;
    }

    public void Next()
    {
        if (CurrentPage + 1 > windows.Count)
        {
            if (ListCycled)
            {
                throw new NotImplementedException();
            }
            return;
        }

        CurrentPage++;
    }

    public void Previous()
    {
        if (CurrentPage - 1 < 0)
        {
            if (ListCycled)
            {
                throw new NotImplementedException();
            }
            return;
        }

        CurrentPage--;
    }

    private void SetActiveWindow(int currentPage)
    {
        foreach (GameObject obj in windows.Where((obj, index) => index != currentPage))
        {
            obj.SetActive(false);
        }
        windows[currentPage].SetActive(true);
    }

    public void Close()
    {
        _currentPage = 0;
        windows.ForEach(obj => obj.SetActive(false));
    }

    public void PopUpEffect()
    {
        transform.localScale = new Vector3(.8f, .8f, 1);
        transform.DOScale(Vector3.one, .33f);
    }
}
