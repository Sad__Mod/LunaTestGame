﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ProgressBarController : MonoBehaviour
{
    /// <summary>
    /// Percentage text
    /// </summary>
    [SerializeField] private Text ProgressText;
    /// <summary>
    /// Prefab that contains ready-to-use mechanic.
    /// </summary>
    [SerializeField] private GameObject ProgressBar;
    /// <summary>
    /// Value, that will change the appearance and progress of the <see cref="ProgressBar">Progress bar</see> prefab and <see cref="ProgressText">progress text's</see> percentage.
    /// </summary>
    [Range(0, 1)] public float Progress = 0.5f;

    /// <summary>
    /// Visual that makes progress bar - what it is.
    /// </summary>
    private Image _progressBarMask;

    private Image GetMask() => ProgressBar.GetComponentsInChildren<Image>().FirstOrDefault(img => img.type == Image.Type.Filled);

    private void Awake() => GetMask();

    private void Update()
    {
        if (ProgressText != null) ProgressText.text = $"{Mathf.RoundToInt(Progress * 100)}%";
        if (ProgressBar == null) return;
        if (_progressBarMask == null)
        {
            var result = GetMask();
            if (result == null) return;
            _progressBarMask = result;
        }
        _progressBarMask.fillAmount = Progress;
    }
}
