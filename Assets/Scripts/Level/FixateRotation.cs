using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class FixateRotation : MonoBehaviour
{
    public TweenerCore<Quaternion, Vector3, QuaternionOptions> Leaner;
    void Start()
    {
        Leaner = transform.DORotate(transform.rotation.eulerAngles, 3).SetEase(Ease.Linear).SetLoops(-1);
    }
}
