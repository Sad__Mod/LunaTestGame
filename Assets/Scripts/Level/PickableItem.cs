using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Core;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PickableItem : MonoBehaviour
{
    public enum PickableType { Coin, Ring }

    [SerializeField] private PickableType Type = PickableType.Coin;
    [SerializeField] private bool RotateOnAwake = false;

    [Header("Sounds")] 
    [SerializeField] private AudioSource PickSound;


    private LevelLogic _level;
    private TweenerCore<Quaternion, Vector3, QuaternionOptions> _animationTweener;
    private Transform _visualsTarget;

    private void Start()
    {
        if (LevelLogic.Instance == null) throw new NullReferenceException($"{nameof(LevelLogic)} is null. This script is required for score management.");
        _level = LevelLogic.Instance;


        _visualsTarget = transform.GetComponentsInChildren<Transform>().First(obj => obj.name == "Visual");
        if (RotateOnAwake) Animate();
    }

    private void Animate()
    {
        switch (Type)
        {
            case PickableType.Coin: _animationTweener = _visualsTarget.DOLocalRotate(new Vector3(0, 0, 360), 3, RotateMode.LocalAxisAdd).SetLoops(-1).SetEase(Ease.Linear); break;
            case PickableType.Ring: throw new NotImplementedException();
            default: throw new ArgumentOutOfRangeException();
        }
    }

    private void OnTriggerEnter(Collider reason)
    {
        if (reason.attachedRigidbody.tag != "Player") return;

        PickSound.Play();

        _level.IncreaseScore(Type);

        GetComponent<Collider>().enabled = false;

        StartCoroutine(Fadeout());
    }

    private IEnumerator Fadeout()
    {
        _visualsTarget.gameObject.GetComponent<MeshRenderer>().material.DOFade(0, 1);
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
    }
}
