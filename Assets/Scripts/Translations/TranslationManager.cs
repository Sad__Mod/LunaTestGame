﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Core;
using Assets.Scripts.Translations.Languages;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Translations
{
    public class TranslationManager : MonoBehaviour
    {
        public enum SupportedLanguages { English, Russian }

        public static TranslationManager Instance;

        /// <summary>
        /// Instance of filled <see cref="TranslationFields"/> object of <see cref="CurrentLanguage">Current language</see>.
        /// </summary>
        public TranslationFields TranslatedText;

        /// <summary>
        /// If language has changed - reload the <see cref="TranslationFields"/> and re-translate all available fields.
        /// </summary>
        public SupportedLanguages CurrentLanguage
        {
            get => _currentLanguage;
            set
            {
                _currentLanguage = value; 
                Localize();
            }
        }
        /// <summary>
        /// This field added to support switching language during Unity Debug (via typing required language. Options are listed in: <see cref="SupportedLanguages"/>, and it is case sensitive).
        /// </summary>
        [SerializeField] private string UnityDebug_CurrentLang = nameof(SupportedLanguages.English);
        /// <summary>
        /// List of all texts, that needs to be translated
        /// </summary>
        private List<Text> _translatableText = new List<Text>();
        /// <summary>
        /// Current language of the application
        /// </summary>
        private SupportedLanguages _currentLanguage;

        private void Awake()
        {
            if (Instance != null) throw new Exception($"Only one {nameof(TranslationManager)} is allowed");
            Instance = this;
        }
        /// <summary>
        /// External method that allows adding translatable text to main collection, for translating all at one.
        /// </summary>
        /// <param name="addedTranslatableTextDictionary"></param>
        public static void Add(List<Text> addedTranslatableTextDictionary) => Instance._translatableText.AddRange(addedTranslatableTextDictionary);

        /// <summary>
        /// Restore current language from save file.
        /// </summary>
        private void Start()
        {
            CurrentLanguage = GameManager.Instance.LevelData.Lang;
            UnityDebug_CurrentLang = CurrentLanguage.ToString();
        }

        /// <summary>
        /// Tracks changes in language field (Unity only)
        /// </summary>
        private void Update()
        {
            if (!Application.isEditor) return;

            if (!Enum.TryParse<SupportedLanguages>(UnityDebug_CurrentLang, false, out var Parsed_UnityDebug_CurrentLang)) return;

            if (Parsed_UnityDebug_CurrentLang != CurrentLanguage) CurrentLanguage = Parsed_UnityDebug_CurrentLang;
        }

        /// <summary>
        /// Main method that responsible for translation assignment.
        /// </summary>
        public void Localize()
        {
            LocaliziblesNullCheck();

            switch (_currentLanguage)
            {
                case SupportedLanguages.English: TranslatedText = Translations_English.TranslatedText; break;
                case SupportedLanguages.Russian: TranslatedText = Translations_Russian.TranslatedText; break;
                default: throw new ArgumentOutOfRangeException();
            }

            _translatableText.Find(o => o.gameObject.name == nameof(TranslatedText.LoadingText)).GetComponent<Text>().text = TranslatedText.LoadingText;
            _translatableText.Find(o => o.gameObject.name == nameof(TranslatedText.GameTips)).GetComponent<Text>().text = TranslatedText.GameTips[GameManager.Instance.GameTipIndex == -1 ? 0 : GameManager.Instance.GameTipIndex];
            
            if (!GameManager.Instance.LevelLoaded) return;
            // template
            // _translatableText.Find(o => o.gameObject.name == nameof(TranslatedText.)).text = TranslatedText.
            _translatableText.Find(o => o.gameObject.name == "MW_1_Header").text = TranslatedText.Tutorial_Headers[0];
            _translatableText.Find(o => o.gameObject.name == "MW_2_Header").text = TranslatedText.Tutorial_Headers[1];
            _translatableText.Find(o => o.gameObject.name == "MW_3_Header").text = TranslatedText.Tutorial_Headers[2];
            _translatableText.Find(o => o.gameObject.name == "MW_4_Header").text = TranslatedText.Tutorial_Headers[3];
            _translatableText.Find(o => o.gameObject.name == "MW_5_Header").text = TranslatedText.Tutorial_Headers[4];
            _translatableText.Find(o => o.gameObject.name == "MW_1_Content").text = TranslatedText.Tutorial_Content[0];
            _translatableText.Find(o => o.gameObject.name == "MW_2_Content").text = TranslatedText.Tutorial_Content[1];
            _translatableText.Find(o => o.gameObject.name == "MW_3_Content").text = TranslatedText.Tutorial_Content[2];
            _translatableText.Find(o => o.gameObject.name == "MW_4_Content").text = TranslatedText.Tutorial_Content[3];
            _translatableText.Find(o => o.gameObject.name == "MW_5_Content").text = TranslatedText.Tutorial_Content[4];
            _translatableText.Find(o => o.gameObject.name == "MW_1_Next").text = TranslatedText.Next;
            _translatableText.Find(o => o.gameObject.name == "MW_2_Next").text = TranslatedText.Next;
            _translatableText.Find(o => o.gameObject.name == "MW_3_Next").text = TranslatedText.Next;
            _translatableText.Find(o => o.gameObject.name == "MW_4_Next").text = TranslatedText.Next;
            _translatableText.Find(o => o.gameObject.name == "MW_5_Next").text = TranslatedText.Next;
            _translatableText.Find(o => o.gameObject.name == "Restart_button_Label").text = TranslatedText.Restart;
            _translatableText.Find(o => o.gameObject.name == "Exit_button_Label").text = TranslatedText.Exit;
            _translatableText.Find(o => o.gameObject.name == nameof(TranslatedText.TimeRemaining)).text = TranslatedText.TimeRemaining;

            if (GameManager.Instance.Level == null) return;

            _translatableText.Find(o => o.gameObject.name == "EndCardText").text = string.Format(
                TranslatedText.EndCard,
                GameManager.Instance.Level.TotalScore, 
                GameManager.Instance.LevelData.Highscore,
                GameManager.Instance.Level.CoinsCollected, 
                GameManager.Instance.Level.RingsPassed + 1);
        }

        /// <summary>
        /// In case of invalid(null) values - drop each of invalid values from general translatable collection.
        /// </summary>
        private void LocaliziblesNullCheck()
        {
            for (int i = _translatableText.Count - 1; i >= 0; i--)
            {
                if (_translatableText[i] == null) _translatableText.Remove(_translatableText[i]);
            }
        }
    }
}
