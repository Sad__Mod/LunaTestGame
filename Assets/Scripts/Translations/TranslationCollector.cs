﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Translations
{
    /// <summary>
    /// <b><u>One per scene</u></b>
    /// <para>After loading, adds translatable text to <see cref="TranslationManager"/> text collection and updates all fields to match the current language.</para>
    /// </summary>
    public class TranslationCollector : MonoBehaviour
    {
        /// <summary>
        /// List of Text objects that needs to be translated.
        /// </summary>
        public List<Text> TranslatableText = new List<Text>();

        /// <summary>
        /// On scene preparing, tells <see cref="TranslationManager"/> that scene has Text that needs to be translated and gives it list of text objects.
        /// </summary>
        private void Awake() => TranslationManager.Add(TranslatableText);
    }
}
