﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Assets.Scripts.Translations.Languages
{
    /// <summary>
    /// Base class for all translation options
    /// </summary>
    public abstract class Translation
    {
        public abstract TranslationFields GetTranslation();
    }

    /// <summary>
    /// Fields that will contain translated text.
    /// <para>Warning: Translatable GameObject should be named <u><b>the same</b></u> as field in this class. No white-space allowed.</para>
    /// <para>OR: GO's could be defined by code during assignment. See <see cref="TranslationManager.Localize()"/> for example.</para>>
    /// </summary>
    public class TranslationFields
    {
        [NotNull] public string LoadingText;
        [NotNull] public List<string> GameTips;
        [NotNull] public List<string> Tutorial_Headers;
        [NotNull] public List<string> Tutorial_Content;
        [NotNull] public string OK;
        [NotNull] public string Next;
        [NotNull] public string Exit;
        [NotNull] public string Restart;
        [NotNull] public string GoToStore;
        [NotNull] public string EndCard;
        [NotNull] public string TimeRemaining;
    }
}
