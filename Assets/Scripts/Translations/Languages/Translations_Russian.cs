﻿using System.Collections.Generic;

namespace Assets.Scripts.Translations.Languages
{
    /// <summary>
    /// Russian translation
    /// </summary>
    public class Translations_Russian : Translation
    {
        public static Translations_Russian Instance;
        public static TranslationFields TranslatedText
        {
            get
            {
                if (_translatedText == null)
                {
                    if (Instance == null) Instance = new Translations_Russian();
                    return _translatedText = Instance.GetTranslation();
                }
                return _translatedText;
            }
        }
        private static TranslationFields _translatedText;

        public Translations_Russian()
        {
            Instance = this;
        }

        /// <summary>
        /// Main filling method
        /// </summary>
        /// <returns><see cref="TranslationFields"/> object, that filled with Russian variant of text.</returns>
        public override TranslationFields GetTranslation()
        {
            return new TranslationFields
            {
                GameTips = new List<string>
                {
                    "Совет 1/3: Загрузка произойдет быстрее, если вы закроете все не нужные на данный момент приложения.",
                    "Совет 2/3: Совет #1 очень полезный.",
                    "Совет 3/3: Совет #2 - истина",
                },
                LoadingText = "Загрузка...",
                Tutorial_Headers = new List<string>
                {
                    "Привет и добро пожаловать в Гонщик Луны!",
                    "Обучение: Управление",
                    "Обучение: Цель игры (1/2)",
                    "Обучение: Цель игры (2/2)",
                    "Вот и всё!"
                },
                Tutorial_Content = new List<string>
                {
                    "Правила просты!\n\n\nНажми <<Дальше>> чтобы продолжить...",
                    "Эта игра использует гироскоп для поворота машины.\n\n\nЭто значит что тебе придётся наклонять свое устройство вправо или влево чтобы разворачиваться.",
                    "Собирай монетки чтобы сделать свой счет больше!",
                    "Проезжай через Кольца чтобы увеличить свой множитель счета!",
                    "Тебе будет дано 30 секунд.\nСобери как можно больше.\n\nРазвлекайся!"
                },
                OK = "ОК!",
                Next = "Дальше",
                Exit = "Выйти",
                Restart = "Хочешь еще?",
                GoToStore = "Загрузить полную версию",
                EndCard = "Демонстрация завершена.\n\nСчет: {0}\nРекорд: {1}\n\nСобрано монет: {2}\nКонечный множитель составлял: x{3}",
                TimeRemaining = "Осталось времени"
            };
        }

        //TODO: Make func (maybe static?) that finds field by name and returns it's translated variant
    }
}