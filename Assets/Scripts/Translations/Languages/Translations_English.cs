﻿using System.Collections.Generic;

namespace Assets.Scripts.Translations.Languages
{
    /// <summary>
    /// English (Default translation)
    /// </summary>
    public class Translations_English : Translation
    {
        public static Translations_English Instance;
        public static TranslationFields TranslatedText
        {
            get
            {
                if (_translatedText == null)
                {
                    if (Instance == null) Instance = new Translations_English();
                    return _translatedText = Instance.GetTranslation();
                }
                return _translatedText;
            }
        }
        private static TranslationFields _translatedText;

        public Translations_English()
        {
            Instance = this;
        }

        /// <summary>
        /// Main filling method
        /// </summary>
        /// <returns><see cref="TranslationFields"/> object, that filled with English variant of text.</returns>
        public override TranslationFields GetTranslation()
        {
            return new TranslationFields
            {
                GameTips = new List<string>
                {
                    "Tip 1/3: Loading will go faster, if you close unnecessary background applications.",
                    "Tip 2/3: Tip #1 is very useful.",
                    "Tip 3/3: Tip #2 is true.",
                },
                LoadingText = "Loading...",
                Tutorial_Headers = new List<string>
                {
                    "Hi, and welcome to Luna's Racer!",
                    "Tutorial: Controls",
                    "Tutorial: Goals (1/2)",
                    "Tutorial: Goals (2/2)",
                    "This is it! That simple!"
                },
                Tutorial_Content = new List<string>
                {
                    "The rules are simple!\n\n\nClick <<Next>> to continue...",
                    "This game uses a gyroscope for turning the car.\n\n\nThis means that you need to tilt the device to the left or right to turn your car.",
                    "Collect coins to make your score higher!",
                    "Get through Rings to increase your highscore multiplier!",
                    "You will be given 30 sec.\nCollect as much as you can.\n\nHave fun!"
                },
                OK = "OK!",
                Next = "Next",
                Exit = "Exit",
                Restart = "Want more?",
                GoToStore = "Get full version",
                EndCard = "Demo ended.\n\nScore: {0}\nHighscore: {1}\n\nWith collected coins: {2}\nand total multiplier: x{3}",
                TimeRemaining = "Time remaining"
            };
        }
    }
}
