﻿using System;
using Assets.Scripts.Translations;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Core
{
    public class SaveManager : MonoBehaviour, ISave
    {
        /// <summary>
        /// Loads and parses the save data from playerPrefs into the <see cref="LevelSave"/> object by Newtonsoft.JsonParser.
        /// </summary>
        /// <returns>If no save data retrieved - new <see cref="LevelSave"/> object, otherwise - parsed <see cref="LevelSave"/> object.</returns>
        public LevelSave GetSave()
        {
            if (PlayerPrefs.HasKey(GameManager.PlayerPrefs_LevelSave))
            {
                var save = JsonConvert.DeserializeObject<LevelSave>(PlayerPrefs.GetString(GameManager.PlayerPrefs_LevelSave));
                return save ?? new LevelSave();
            }
            return new LevelSave();
        }

        /// <summary>
        /// Converts <see cref="LevelSave"/> object to Json string that can be saved by PlayerPrefs.
        /// </summary>
        /// <param name="saveFile"><see cref="LevelSave"/> object (Recommended to use <see cref="GameManager.LevelData"/> for minimizing save branching/variation).</param>
        public void SetSave(LevelSave saveFile) => PlayerPrefs.SetString(GameManager.PlayerPrefs_LevelSave, JsonConvert.SerializeObject(saveFile));

        /// <summary>
        /// Clears any saved data if there were any.
        /// </summary>
        public void ClearSave() {
            if (PlayerPrefs.HasKey(GameManager.PlayerPrefs_LevelSave))
                PlayerPrefs.DeleteKey(GameManager.PlayerPrefs_LevelSave);
        }

        /// <summary>
        /// Save file structure.
        /// </summary>
        public class LevelSave
        {
            public DateTime LastSaveTime = DateTime.Now;
            public float Highscore = 0;
            public TranslationManager.SupportedLanguages Lang = (TranslationManager.Instance == null ? TranslationManager.SupportedLanguages.English : TranslationManager.Instance.CurrentLanguage);
        }
    }

    interface ISave
    {
        SaveManager.LevelSave GetSave();
        void SetSave(SaveManager.LevelSave saveFile);
        void ClearSave();
    }
}