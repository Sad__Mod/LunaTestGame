﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using Assets.Scripts.Translations;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts.Core
{
    public class LevelLogic : MonoBehaviour
    {
        public static LevelLogic Instance;

        #region Game logic fields

        [Header("Canvases")]
        public CanvasGroup UI_root;
        [SerializeField] private CanvasGroup FadeToWhiteCanvasGroup, UI_Gameplay;

        [Header("UI")]
        [SerializeField] private MessageWindowController TutorialWindow;
        [SerializeField] private Text GameStartCountdown, RemainingGameTimeText;
        [SerializeField] private GameObject GoToStoreButton, AfterFadeButtons;

        [Header("Level")]
        [SerializeField] private GameObject AppearOnStart;

        private Stopwatch _stopwatch;
        private Coroutine _stopwatchCoroutine;

        #endregion

        #region SoundManagement

        [Header("Sounds")] 
        [SerializeField] private AudioSource GameplayMusic;
        [SerializeField] private AudioSource UIPing, CountdownPing, CountdownPingEnd;
        [SerializeField] private AudioMixerGroup CarMixerGroup;

        #endregion

        private float _coinsCollected = 0, _ringsPassed = 0, _totalScore = 0;

        public float CoinsCollected => _coinsCollected;

        public float RingsPassed => _ringsPassed;

        public float TotalScore => _totalScore;

        public void Awake()
        {
            if (Instance != null) throw new Exception($"Only one {nameof(LevelLogic)} is allowed");
            Instance = this;
            if (GameManager.Instance == null) throw new NullReferenceException($"{nameof(GameManager)} is null. Core script is required to play. Restart game!");
            GameManager.Instance.Level = Instance;
            UI_root.DOFade(1, GameManager.Instance.FadeInOutTime);
        }

        /// <summary>
        /// Tells level that GameManager has finished all preparations, and ready to show gameplay
        /// </summary>
        public void OnReady()
        {
            Debug.Log("OnReady()");

            GameplayMusic.DOFade(1, .5f);

            if (GameManager.ShowTutorial)
            {
                ShowTutorial();
            }
            else
            {
                StartGame();
            }
        }

        private void ShowTutorial()
        {
            TutorialWindow.CurrentPage = 0;
            TutorialWindow.PopUpEffect();
        }

        public void StartGame() => _stopwatchCoroutine = StartCoroutine(GameLoop());

        /// <summary>
        /// Timer that counts when to finish demo
        /// </summary>
        private IEnumerator GameLoop()
        {
            UI_Gameplay.gameObject.SetActive(true);
            UI_Gameplay.DOFade(1, .5f);
            GameStartCountdown.gameObject.SetActive(true);

            yield return new WaitForSeconds(.5f);

            _stopwatch = Stopwatch.StartNew();

            var _lastPing = -1;

            while (_stopwatch.ElapsedMilliseconds < 3000)
            {
                GameStartCountdown.text = $"{2 - _stopwatch.Elapsed.Seconds}.{(1000 - _stopwatch.Elapsed.Milliseconds) / 100:0}";
                if (_lastPing != _stopwatch.Elapsed.Seconds)
                {
                    CountdownPing.Play();
                    _lastPing = _stopwatch.Elapsed.Seconds;
                }
                yield return !_stopwatch.IsRunning;
            }

            CountdownPingEnd.Play();
            GameStartCountdown.gameObject.SetActive(false);
            AppearOnStart.SetActive(true);

            _stopwatch = Stopwatch.StartNew();
            while (_stopwatch.ElapsedMilliseconds < 30000)
            {
                RemainingGameTimeText.text = $"{30 - _stopwatch.Elapsed.Seconds:0}:{(1000 - _stopwatch.Elapsed.Milliseconds) / 10:00} s.";
                yield return !_stopwatch.IsRunning;
            }

            _stopwatch.Stop();

            RemainingGameTimeText.text = $"30.00 ms.";

            // Player won't click the screen if will be holding forward button.
            //GoToStoreButton.SetActive(true);
            StartCoroutine(OnGoToStoreClick());
        }

        /// <summary>
        /// Function that should be called via click anywhere on the screen, after main gameplay.
        /// </summary>
        public void GoToStore()
        {
            Debug.Log($"GoToStore() invoked!");
            StartCoroutine(OnGoToStoreClick());
        }

        /// <summary>
        /// OnClick() sequenced handler
        /// </summary>
        private IEnumerator OnGoToStoreClick()
        {
            GoToStoreButton.SetActive(false);
            FadeToWhiteCanvasGroup.DOFade(1, 5);
            yield return new WaitForSeconds(6.5f);
            AfterFadeButtons.SetActive(true);

            if (_totalScore > GameManager.Instance.LevelData.Highscore) GameManager.Instance.LevelData.Highscore = _totalScore;
            TranslationManager.Instance.Localize();
        }

        public void Exit() => Application.Quit();

        /// <summary>
        /// If player chose to restart the "game" after it finished - update save file and reload the scene
        /// </summary>
        public void Restart()
        {
            var save = GameManager.Instance.LevelData;
            save.Lang = TranslationManager.Instance.CurrentLanguage;

            GameManager.UnloadScene(Scenes.GameplayLevel);
            GameManager.LoadScene(Scenes.GameplayLevel);
        }

        /// <summary>
        /// Save gameData (save file) on scene reload or app exit
        /// </summary>
        private void OnDestroy(){
            GameManager.Instance.SaveManager.SetSave(GameManager.Instance.LevelData);
        }

        public void TutorialCompleted(bool isCompleted = true)
        {
            GameManager.ShowTutorial = !isCompleted;
        }

        public void IncreaseScore(PickableItem.PickableType type)
        {
            if(!_stopwatch.IsRunning) return;

            switch (type)
            {
                case PickableItem.PickableType.Coin:
                {
                    _coinsCollected++;
                    _totalScore += 100 * (_ringsPassed + 1);
                } break;
                case PickableItem.PickableType.Ring:
                {
                    _ringsPassed++;
                } break;
                default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}