﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Translations;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Core
{
    [RequireComponent(typeof(SaveManager))]
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        public LevelLogic Level;
        public SaveManager.LevelSave LevelData;

        #region Loading components

        public bool LevelLoaded { get; private set; }
        public float FadeInOutTime => _fadeInOutTime;

        [HideInInspector] public int GameTipIndex = -1;
        [SerializeField] private CanvasGroup CanvasFaderMain;
        [SerializeField] private ProgressBarController LoadingProgressBar;
        [SerializeField] private float _fadeInOutTime;
        [SerializeField] private Text GameTipPlace;
        private List<TweenerCore<float, float, FloatOptions>> LoadingTweens = new List<TweenerCore<float, float, FloatOptions>>();
        private Coroutine _loadingTracker;

        #endregion

        public SaveManager SaveManager;

        #region PlayerPrefs Keys

        public static readonly string PlayerPrefs_LevelSave = "LevelSave";

        #endregion

        private List<Scene> LoadedScenes = new List<Scene>();
        private List<AsyncOperation> LoadingScenes = new List<AsyncOperation>();

        #region SoundManagement

        [SerializeField] private AudioListener _coreListener;

        [SerializeField]private AudioSource _loadingMusicSource;
        private TweenerCore<float, float, FloatOptions> _loadingMusicTweener;

        #endregion

        #region GameComponents

        // Allow only turning this param off
        public static bool ShowTutorial
        {
            get => Instance._showTutorial;
            set
            {
                if (Instance._showTutorial == false) return;
                Instance._showTutorial = value;
            }
        }

        [SerializeField] private bool _showTutorial = true;

        #endregion

        private void Awake()
        {
            if (Instance != null) throw new Exception($"Only one {nameof(GameManager)} is allowed");
            Instance = this;
            LoadedScenes.Add(SceneManager.GetActiveScene());
            SaveManager = gameObject.AddComponent<SaveManager>();
            CanvasFaderMain.alpha = 1;
        }

        // TODO: Make performance setting (FPS, Quality, Sound)
        void Start()
        {
            if (Application.systemLanguage == SystemLanguage.Russian)
                TranslationManager.Instance.CurrentLanguage = TranslationManager.SupportedLanguages.Russian;
            LevelData = SaveManager.GetSave();
            SceneManager.sceneLoaded += SceneManagerOnsceneLoaded;
            LoadScene(Scenes.GameplayLevel);

        }

        /// <summary>
        /// When SceneLoaded event triggers - add loaded scene to the local list and run translation func for loaded scene
        /// </summary>
        /// <param name="scene">Loaded scene (passed by delegate param)</param>
        /// <param name="mode">Loaded scene mode (Single or Additive) (passed by delegate param)</param>
        private void SceneManagerOnsceneLoaded(Scene scene, LoadSceneMode mode)
        {
            LoadedScenes.Add(scene);
            SceneManager.SetActiveScene(scene);
            TranslationManager.Instance.Localize();

            _coreListener.enabled = scene.buildIndex != (int) Scenes.GameplayLevel;

            if (scene.buildIndex == (int)Scenes.GameplayLevel)
            {
                DOTween.Kill(Instance._loadingMusicSource, _loadingMusicTweener.id);
                _loadingMusicTweener = _loadingMusicSource.DOFade(0, .25f);
            }
        }

        /// <summary>
        /// Main func for loading a scenes via code
        /// </summary>
        /// <param name="scene">What scene should we load? (Use <see cref="Scenes"/> enum to specify this param).</param>
        /// <param name="mode">Should this scene be the only one that loaded, or it will be added to already loaded ones. Use <see cref="LoadSceneMode"/> to specify.</param>
        /// <example><code>LoadScene(Scenes.GameManager, LoadSceneMode.Single)</code></example>
        /// <example><code>LoadScene(Scenes.GameplayLevel, LoadSceneMode.Additive)</code></example>
        /// <example><code>LoadScene(Scenes.GameplayLevel)</code></example>
        public static void LoadScene(Scenes scene, LoadSceneMode mode = LoadSceneMode.Additive)
        {
            if (mode == LoadSceneMode.Single) Instance.LoadedScenes.Clear();

            switch (scene)
            {
                case Scenes.GameManager: {
                    Instance.LoadingScenes.Add(SceneManager.LoadSceneAsync((int)scene, mode));
                } break;
                case Scenes.GameplayLevel: {
                    Instance.LoadingScenes.Add(SceneManager.LoadSceneAsync((int)scene, mode));
                    Instance.ShowLoadingScreen();
                } break;
                default: throw new ArgumentOutOfRangeException(nameof(scene), scene, null);
            }
        }

        /// <summary>
        /// Tells GameManager that that scene is no longer required, and unloades all of it's scene objects from memory.
        /// </summary>
        /// <param name="scene"></param>
        public static void UnloadScene(Scenes scene)
        {
            if (scene == Scenes.GameplayLevel) Instance.LevelLoaded = false;
            foreach (var validLoadedScene in Instance.LoadedScenes.Where(loadedScene => loadedScene.buildIndex == (int)scene)) SceneManager.UnloadSceneAsync(validLoadedScene);
        }

        /// <summary>
        /// Invoked if any scene started it's loading. Providing info for player, that game not frozen.
        /// </summary>
        private void ShowLoadingScreen()
        {
            Instance._loadingMusicTweener = Instance._loadingMusicSource.DOFade(1, .25f);
            LoadingTweens.Add(CanvasFaderMain.DOFade(1, FadeInOutTime));
            _loadingTracker = StartCoroutine(StartLoadTracking());
        }

        /// <summary>
        /// Main loop that updates LoadingScreen and keeps it Up-to-Date. When loading is done - Hides LoadingScreen that is no longer required,
        /// and after finishing hiding - tells LevelLogic that all of animations has passed, and player is ready to play.
        /// </summary>
        private IEnumerator StartLoadTracking()
        {
            var startTime = Time.realtimeSinceStartup;

            do
            {
                if (Time.realtimeSinceStartup - startTime > 3)
                {
                    ChangeTip();
                    startTime = Time.realtimeSinceStartup;
                }
                LoadingProgressBar.Progress = LoadingScenes.Sum(scene => scene.progress);
                yield return null;
            } while (LoadingScenes.All(scene=> scene.isDone));
            LevelLoaded = true;
            HideLoadingScreen();
            yield return new WaitForSeconds(FadeInOutTime - .5f);
            LevelLogic.Instance.OnReady();
        }

        /// <summary>
        /// If loading takes too much time - tips will switch one after another while LoadingScreen visible.
        /// </summary>
        private void ChangeTip()
        {
            if (GameTipIndex == -1 || GameTipIndex + 1 == TranslationManager.Instance.TranslatedText.GameTips.Count)
            {
                GameTipIndex = 0;
                GameTipPlace.text = TranslationManager.Instance.TranslatedText.GameTips[GameTipIndex];
            }
            else
            {
                GameTipIndex++;
                GameTipPlace.text = TranslationManager.Instance.TranslatedText.GameTips[GameTipIndex];
            }

            TranslationManager.Instance.Localize();
        }

        /// <summary>
        /// Updates LoadingScreen last time and hides LoadingScreen smoothly.
        /// </summary>
        private void HideLoadingScreen()
        {
            LoadingProgressBar.Progress = 1;
            LoadingTweens.ForEach(tween => DOTween.Kill(this, tween.id));
            LoadingTweens.Clear();
            LoadingTweens.Add(CanvasFaderMain.DOFade(0, FadeInOutTime));
        }
    }

    /// <summary>
    /// Scenes list. Should match index and name with In-Unity BuildScenes list
    /// </summary>
    public enum Scenes { GameManager, GameplayLevel }
}